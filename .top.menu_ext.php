<?

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
CModule::IncludeModule("iblock");
global $APPLICATION;

$arFilter = array('IBLOCK_ID' => 2); // выберет потомков без учета активности
$rsSect = CIBlockSection::GetList(array('left_margin' => 'asc', 'sort' => 'asc'),$arFilter);
while ($arSect = $rsSect->GetNext())
{
    $aMenuLinks[] = [
        $arSect["NAME"],
        $arSect["SECTION_PAGE_URL"],
        [],
        [
            "IS_PARENT" => !empty($arSect["IBLOCK_SECTION_ID"]) || ($arSect["ID"] == 9) ? 0 : 1,
            "DEPTH_LEVEL" => $arSect["DEPTH_LEVEL"]           
        ]
    ];
    //echo "<pre>"; var_dump($arSect); echo "</pre>";
}
//echo "<pre>"; var_dump($aMenuLinks); echo "</pre>";
//$aMenuLinks = array_merge($aMenuLinks, $aMenuLinksExt);
?>