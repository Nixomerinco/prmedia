<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$themeClass = isset($arParams['TEMPLATE_THEME']) ? ' bx-'.$arParams['TEMPLATE_THEME'] : '';

?>

<div class="container-fluid pb-4 pt-4 paddding news-list<?=$themeClass?>">
	<div class="container paddding">
		<div class="row mx-0">
		<div class="col-md-12 animate-box" data-animate-effect="fadeInLeft">
			<!--<div>
				<div class="fh5co_heading fh5co_heading_border_bottom py-2 mb-4"><?//$APPLICATION->ShowTitle()?></div>
			</div>-->
			<? foreach($arResult["ITEMS"] as $arItem): ?>
				<div class="row pb-4">
					<div class="col-md-5">
						<div class="fh5co_hover_news_img">
							<div class="fh5co_news_img">
								<a href="<?=$arItem["DETAIL_PAGE_URL"]; ?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["SAFE_SRC"]; ?>" alt="<?=$arItem["ALT"]; ?>"/></a>
							</div>
						</div>
					</div>
					<div class="col-md-7 animate-box">
						<a href="<?=$arItem["DETAIL_PAGE_URL"]; ?>" class="fh5co_magna py-2"><?=$arItem["NAME"]; ?></a> 
						<a href="<?=$arItem["DETAIL_PAGE_URL"]; ?>" class="fh5co_mini_time py-3"><?=$arItem["DISPLAY_ACTIVE_FROM"]; ?> </a>
						<div class="fh5co_consectetur"> 
							<?=$arItem["PREVIEW_TEXT"]; ?>
						</div>
					</div>
				</div>
			<? endforeach; ?>
		</div>
	</div>
		<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
			<?=$arResult["NAV_STRING"]?>
		<?endif;?>
	</div>
</div>