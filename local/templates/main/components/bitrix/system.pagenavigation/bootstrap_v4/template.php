<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

/**
 * @var array $arResult
 * @var array $arParam
 * @var CBitrixComponentTemplate $this
 */

$this->setFrameMode(true);


?>
<? if($arResult["NavPageCount"]>1): ?>
<div class="row mx-0 animate-box" data-animate-effect="fadeInUp">
	<div class="col-12 text-center pb-4 pt-4">
		<!--Back-->
		<?if ($arResult["NavPageNomer"] > 1):
			if($arResult["bSavePage"]):?>
				<a class="btn_mange_pagging" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>"><i class="fa fa-long-arrow-left"></i></a>
			<?else:
				if ($arResult["NavPageNomer"] > 2):?>
				<a class="btn_mange_pagging" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>"><i class="fa fa-long-arrow-left"></i></a>
				<?else:?>
				<a class="btn_mange_pagging" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><i class="fa fa-long-arrow-left"></i></a>
				<?endif;
			endif;
		else:?>
			<span class="btn_mange_pagging"><i class="fa fa-long-arrow-left"></i></span>
		<?endif;?>
		<!--Inter-->
		<? for($i=1;$i<=$arResult["NavPageCount"];$i++): ?>
		<a href="?PAGEN_1=<?=$i?>" class="btn_pagging"><?=$i?></a>
		<? endfor; ?>
		<!--Forward-->
		<?if($arResult["NavPageNomer"] < $arResult["NavPageCount"]):?>
			<a class="btn_mange_pagging" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>"><i class="fa fa-long-arrow-right"></i></a>
		<?else:?>
			<span class="btn_mange_pagging"><i class="fa fa-long-arrow-right"></i></span>
		<?endif;?>
	</div>
</div>
<? endif; ?>