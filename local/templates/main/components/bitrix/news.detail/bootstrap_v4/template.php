<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$themeClass = isset($arParams['TEMPLATE_THEME']) ? ' bx-'.$arParams['TEMPLATE_THEME'] : '';
CUtil::InitJSCore(array('fx'));
?>
<div class="single news-detail<?=$themeClass?>">
	<div class="mb-3" id="<?echo $this->GetEditAreaId($arResult['ID'])?>">
		<div id="fh5co-title-box" style="background-image: url(<?=$arResult["DETAIL_PICTURE"]["SRC"]?>);" data-stellar-background-ratio="0.5">
			<div class="overlay"></div>
			<div class="page-title">
				<span><?echo $arResult["DISPLAY_ACTIVE_FROM"]?></span>
				<h2><?=$arResult["NAME"]?></h2>
			</div>
		</div>
		<div id="fh5co-single-content" class="container-fluid pb-4 pt-4 paddding">
			<div class="container paddding">
				<div class="row mx-0">
					<div class="col-md-12 animate-box" data-animate-effect="fadeInLeft">
						<?=$arResult["DETAIL_TEXT"];?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
