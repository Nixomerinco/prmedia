<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="container-fluid bg-faded fh5co_padd_mediya padding_786">
    <div class="container padding_786">
        <nav class="navbar navbar-toggleable-md navbar-light ">
            <button class="navbar-toggler navbar-toggler-right mt-3" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="Toggle navigation"><span class="fa fa-bars"></span></button>
            <a class="navbar-brand" href="#"><img src="<?=SITE_TEMPLATE_PATH?>/images/logo.png" alt="img" class="mobile_logo_width"/></a>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                <?
                $previousLevel = 0;
                foreach($arResult as $item): ?>
                    <?if ($previousLevel && $item["PARAMS"]["DEPTH_LEVEL"] < $previousLevel):?>
                        <?=str_repeat("</div></li>", ($previousLevel - $item["PARAMS"]["DEPTH_LEVEL"]));?>
                    <?endif?>

					<? if($item["PARAMS"]["IS_PARENT"]): ?>

                        <? if($item["PARAMS"]["DEPTH_LEVEL"] == 1): ?>
                            <li class="nav-item dropdown">       
                                <a href="<?= $item["LINK"]?>" class="nav-link dropdown-toggle" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <?=$item["TEXT"]?>
                                </a>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink_1">
                        <? endif; ?>
                    
					<? else: ?>
                    
                        <? if($item["PARAMS"]["DEPTH_LEVEL"] > 1): ?>
                                <a class="dropdown-item" href="<?= $item["LINK"]?>">><?=$item["TEXT"]?></a>
                        <? else: ?>
                            <li class="nav-item <?= $item["SELECTED"] ? "active" : ""?>">
                                <a class="nav-link" href="<?= $item["LINK"]?>"><?=$item["TEXT"]?></a>
                            </li>
                        <? endif; ?>
                    <?endif;?>

                    <?$previousLevel = $item["PARAMS"]["DEPTH_LEVEL"];?>
                <? endforeach; ?>
                </ul>
            </div>
        </nav>
    </div>
</div>