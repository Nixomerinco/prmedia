<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
?>
	<div class="container-fluid fh5co_footer_bg pb-3">
		<div class="container animate-box">
			<div class="row">
				<div class="col-12 spdp_right py-5"><img src="<?=SITE_TEMPLATE_PATH?>/images/white_logo.png" alt="img" class="footer_logo"/></div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>

	<div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="fa fa-arrow-up"></i></a>
	</div>
</body>
</html>