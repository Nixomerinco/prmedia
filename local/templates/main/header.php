<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!DOCTYPE html>
<html>
<head>
<link rel="shortcut icon" type="image/x-icon" href="<?=SITE_TEMPLATE_PATH?>/favicon.ico" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<?$APPLICATION->ShowMeta("robots")?>
<?$APPLICATION->ShowMeta("keywords")?>
<?$APPLICATION->ShowMeta("description")?>
<title><?$APPLICATION->ShowTitle()?></title>
<?$APPLICATION->ShowHead();?>
<?IncludeTemplateLangFile(__FILE__);?>
<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/colors.css" />
<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/print.css" media="print" />
<link href="<?=SITE_TEMPLATE_PATH?>/css/media_query.css" rel="stylesheet" type="text/css"/>
<link href="<?=SITE_TEMPLATE_PATH?>/css/bootstrap.css" rel="stylesheet" type="text/css"/>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
		integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<link href="<?=SITE_TEMPLATE_PATH?>/css/animate.css" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
<link href="<?=SITE_TEMPLATE_PATH?>/css/owl.carousel.css" rel="stylesheet" type="text/css"/>
<link href="<?=SITE_TEMPLATE_PATH?>/css/owl.theme.default.css" rel="stylesheet" type="text/css"/>
<!-- Bootstrap CSS -->
<link href="<?=SITE_TEMPLATE_PATH?>/css/style_1.css" rel="stylesheet" type="text/css"/>
<!-- Modernizr JS -->
<script src="<?=SITE_TEMPLATE_PATH?>/js/modernizr-3.5.0.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/owl.carousel.min.js"></script>
<!--<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
        integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
        crossorigin="anonymous"></script>
<!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
        integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
        crossorigin="anonymous"></script>-->
<script src="<?=SITE_TEMPLATE_PATH?>/js/bootstrap.js">
</script>
        
<!-- Waypoints -->
<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.waypoints.min.js"></script>
<!-- Main -->
<script src="<?=SITE_TEMPLATE_PATH?>/js/main.js"></script>
<style>
@media only screen and (min-width: 768px) {
 .dropdown:hover .dropdown-menu {
 display: block;
 margin-top: 0;
 }
}
</style>
</head>
<body>	
<div id="panel"><?$APPLICATION->ShowPanel();?></div>
<!--Header witout menu-->
<div class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-3 fh5co_padding_menu">
                <a href="/"><img src="<?=SITE_TEMPLATE_PATH?>/images/logo.png" alt="img" class="fh5co_logo_width"/></a>
            </div>
            <div class="col-12 col-md-9 align-self-center fh5co_mediya_right">
                <div class="text-center d-inline-block dropdown">
                    <a href="" class="fh5co_display_table" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" >
                        <div class="fh5co_verticle_middle"><i class="fa fa-user"></i></div>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink_1">
                        <?
                        global $USER;
                        if ($USER->IsAuthorized()):
                        ?>
                        <a class="dropdown-item" href="http://prmedia/?logout=yes">Āūéņč</a>
                        <? else: ?>
                        <a class="dropdown-item" href="http://prmedia/auth.php">Āīéņč</a>
                        <a class="dropdown-item" href="http://prmedia/registration.php">Ēąšåćčńņščšīāąņüń’</a>
                        <? endif; ?>
                    </div>
                </div>
                <?$APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_SHOW" => "file", //   
                        "AREA_FILE_SUFFIX" => "",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => SITE_TEMPLATE_PATH."/include/social_links.php" //   
                    )
                );?>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<!--Menu-->
<?
$APPLICATION->IncludeComponent(
    "bitrix:menu", 
    "personal_tab", 
    Array(
        "ROOT_MENU_TYPE" => "top",
        "MAX_LEVEL" => "1",
        "USE_EXT" => "Y"
    )
);
?>